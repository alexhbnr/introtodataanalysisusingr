---
title: "Introduction to the tidyverse suit and data import"
author: "Alex Hübner"
date: "`r format(Sys.time(), '%b %d, %Y')`"
output:
    html_document:
        toc: true
---

<style type="text/css">
body {
  font-size: 18px;
  text-align: justify;
} 

img {
    margin-left: auto;
    margin-right: auto;
    display: block;
}

.caption {
    font-weight: bold;
    text-align: center; 
}

pre {
  font-size: 18px;
  background-color: #DDDDDD;
}

code {
  background-color: #DDDDDD;
}

title {
    display: block
    margin-left: auto
    margin-right: auto
}

div.question {
    background-color: #99DDFF;
    border-radius: 5px;
    padding: 15px;
}
</style>

```{r libraries, echo=F}
library(knitr)
opts_chunk$set(echo=T, warning=F, message=F, dpi=150)
```

# Introduction to the tidyverse package suit

Data analysis is a wide field with many different aspects depending on the input data that are
available, its format, and the resource questions we want to answer. However, most analyses require
a subset if not all steps shown in this schematic:

![Data analysis workflow as introduced by "R for data science"](https://d33wubrfki0l68.cloudfront.net/571b056757d68e6df81a3e3853f54d3c76ad6efc/32d37/diagrams/data-science.png)

Data analysis starts with the import of your data into your working environment, which in this
course will be R. After the import, we have to clean up the data so that we can use it as input in
the most crucial part of data analysis, the exploration of the data.

This course will cover all of the aspects shown in this cartoon in the next five weeks and will
build heavily on the groundwork done by the Hadley Wickham, Garrett Grolemund *et al.* that not just
provided a [great, freely available resource](http://r4ds.had.co.nz) for data analysis but also with
a set of tools summarised in the **package suit** [tidyverse](https://www.tidyverse.org/packages/).

**Tidyverse** is a collection of R packages that were initially introduced by [Hadley
Wickham](http://hadley.nz/) that share a common data representation and API design. While most, if
not all, of the things shown in this course can be achieved using standard base R functions, the
similar grammar (e.g. using verbs explicitly naming the use case of the function as function names)
makes it easy to remember the commands and combine functions from different packages with each
other.

While most people have heard at least of one package, [ggplot2](https://ggplot2.tidyverse.org),
there are a few more functions that I would like to briefly introduce. A complete overview can be
found [here](https://www.tidyverse.org/packages/):

  - **[readr](https://readr.tidyverse.org/)**: fast and user friendly way to read rectangular data
    from file
  - **[tibble](https://tibble.tidyverse.org/)**: provides a modern interpretation of the data
    structure data.frame
  - **[tidyr](https://tidyr.tidyverse.org/)**: provides a set of function to tidy your data so that
    has a consistent form (e.g. no missing data)
  - **[dplyr](https://dplyr.tidyverse.org/)**: provides a set of verbs that serve as functions to
    manipulate your data
  - **[stringr](https://stringr.tidyverse.org/)**: set of functions to manipulate strings
  - **[forcats](https://forcats.tidyverse.org/)**: set of functions to handling categorical data as
    factors
  - **[purrr](https://purrr.tidyverse.org/)**: extension of the functional programming toolkit of R
    to avoid using for-loops
  - **[ggplot2](https://ggplot2.tidyverse.org/)**: library to generate graphics in a declarative way

We will look at each of them in detail during the course. In order to use them, we need to load them
at the beginning of our analysis using `library(<package>)` and replacing the placeholder with the
name of the package of choice. However, since we typically make use of almost all of them during our
analysis, it is easiest to load all of them at once:

```{r tidyverse}
library(tidyverse)
```

**Note**: If we load a new package into our R workspace, all public, i.e. visible for the user,
functions will be registered in the R workspace. If **another function with the same name**, e.g.
`select()`, has been already registered before, R will replace the old definition with the one of
the new package. Since function names quite often have similar names across different packages, this
happens rather often. In order to access the old function, we would also have to specify the package
that is providing the function using a double colon notation, e.g. `dplyr::select()`. This would
tell R to choose the function `select()` from the package `dplyr`.

To avoid such a behaviour for the commonly used functions of the tidyverse package, **you should
always try to load the tidyverse package as late as possible**.

Most of the people that contribute to tidyverse are either employees or associated with the company
**RStudio** that provides the text editor. Therefore, it makes sense to use RStudio, which has many
special features regarding the tidyverse packages implemented, to gain the most out of the packages.

Before starting with actual first step of the data, the data import, I would like to introduce three
aspects of the tidyverse universe that we will use heavily throughout the course: [tibbles](#tibbles),
[R Markdown](#rmarkdown), and [pipes](#magrittr)

# Tibbles {#tibbles}

Almost all objects that we will work with using the functions provided by the package
`library(tidyverse)` will be of class `tibble`. **Tibbles** are modern adaptations of the original
data frame data structure and in most cases behave like them.

The two main differences compared to data frames are **printing the output** and **subsetting**
them. Tibbles have a altered **print** method that by default does not print the whole data, but
**only the first 10 lines** of it, similar like the function `head()`. Further, we **receive
additional information** borrowed from the function `str()`, e.g. the class of each column, and
the dimensions of the table. For subsetting data, tibbles are **more strict** and trigger warnings
if a column that is supposed to be accessed is missing. Further, it **does not perform partial
matching of the column names**, when selecting columns by name. Additional features are that there
are no automatic conversions of strings into factors and column names will not be adapted to R's
variable naming convention.

In order to create a tibble from scratch, we can use the function `tibble()`:

```{r tibble1}
set.seed(1)
df <- tibble(individual = c("A", "B", "C", "D"),
             height = rnorm(4, mean = 1.8, sd = 0.1),
             weight = rnorm(4, mean = 80, sd = 15),
             control = c(T, F, F, T))
df
```

In order to convert a data frame object to tibble, we can use the function `as_tibble()` and for
converting a tibble into a data frame `as.data.frame()`. 

## tl;dr

  - almost all tidyverse function either create or work on the R class **tibble**, a modern
    adaptation of the data structure data frame
  - tibbles provide an improved printing function for large data sets and enforce verbose coding
  - using `as.data.frame()` or `as_tibble()`, tibbles and data frames can be converted into each
    other

## Resources

  - chapter 10 of the book "R for data science": https://r4ds.had.co.nz/tibbles.html
  - vignette page: https://tibble.tidyverse.org/

# R Markdown {#rmarkdown} 

**R Markdown** will be the basis of all material provided in this course and should be the essence of
your daily data analysis in R. Therefore, I want to take a step back and briefly introduce
R Markdown.

The term **R Markdown** was first introduced by the R package `library(knitr)` in 2015 and described
an extension of the **Markdown** format to include and automatically run chunks of R code directly from
the same document. Markdown was invented early by the blogger [John
Gruber](https://daringfireball.net/projects/markdown/) and described a parser of annotated text that
can be directly converted into working HTML code.

HTML is the basic markup language of the internet (pun intended when naming the format) but has a
rather bulky syntax. Therefore, John Gruber declared an alternative, simpler syntax. For example,
instead of highlighting text in bold using `<b>text</b>`, double asterisks are used: `**text**`. A
program later parses this text and replaces the double asterisks with the actual HTML code.

The original parser of the language was somewhat simplistic and did not allow to create technical
documents, e.g. as it can be done with the language LaTeX. In order to enable this, a different
program, **pandoc**, was created that is nowadays at the conversion of Markdown files to all kinds
of other formats.

R Markdown builds upon this system, by introducing a special syntax of code chunks that can include
code from many different languages (see
[here](https://bookdown.org/yihui/rmarkdown/language-engines.html)) next to R. Therefore, R Markdown
allows us to **store both our code, the description of the methods, the results, and interpretation
of the results in a single file**. This provides a proper documentation of the experiment and
increases the probability that the results can be reproduced in the future. Due to its plain text
format and simple syntax, it can be opened in any text editor and keeps a **high readability**.

A R Markdown document consists out of two parts: the **header** describing general meta information
of the document like the author, the date etc. and defines the output format; and the **body**
containing the actual information on the experiment. A simple **header** for producing a report in
HTML format can look like this:

```{r rmarkdown1, eval=F}
---
title: "Hello R Markdown"
author: "Awesome Me"
date: "2019-10-30"
output: html_document
---
```

The header is defined as the code block between `---` (three dashes). It typically contains an
informative title and information on the author and the date. The last part is defining the output
format whose definitions are provided by RStudio. There are twelve output format available that can
be mainly grouped into two categories plus interactive documents:

  * reports in either HTML, PDF, or Word format
  * presentations in either HTML slides, LaTeX Beamer presentation or even Powerpoint
  * interactive documents: Shiny

All remaining code is considered as the body:
  
```{r, out.height = 400, out.width = 512, echo = F}
library(png)
library(grid)
body_example <- readPNG("./RMarkdown_body_example.png")
grid.raster(body_example)
```

If we want to indicate to R Markdown that we want to define a chunk of code, we start the block using
three backticks followed by curly brackets and the name of the programming language. In this case,
we generated a code chunk for the language R indicated by the lowercase **r**. In this code chunk,
we can work as in any other regular R script. In RStudio, we can create this code block with the
shortcut `Ctrl+Alt+I` on Windows and Linux and `CMD+Option+I` on Mac.

Everything outside this code block is interpreted as regular text that is annotated using Markdown
syntax. The only exception here is the so-called inline R code block started by a single backtick
followed directly by the language name. These inline R code blocks are very useful, when preparing
reports and wanting to have automatically the results of the code presented in the text, e.g. when
stating the number of observations in a table that you just loaded.

R Markdown is a great tool that will help you to document your data analysis better and help you
to easily and quickly communicate your results with your colleagues and collaborators. However,
since it is so powerful, we will only scrape the bare surface of it at the moment. We will go in
more detail later when we talk about communicating results.

## tl;dr

  - R Markdown is an extension of the original Markdown format to allow to have the code, the
    description of the methods, the results, and interpretation of the results next to each other in
    a single file
  - multiple output formats are available to communicate your results (will focus on the HTML
    document in this course)
  - all assignments are due in R Markdown format

## Resources

  - chapter 27 of the book "R for data science" on the R Markdown format:
    https://r4ds.had.co.nz/r-markdown.html
  - chapter 29 of the book "R for data science" on the output formats of R Markdown:
    https://r4ds.had.co.nz/r-markdown-formats.html
  - chapter 2 of the book "R Markdown: The Definitive Guide" introducing the basics of the language:
    https://bookdown.org/yihui/rmarkdown/basics.html
  - RStudio R Markdown cheat sheet: https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf


# Piping using magrittr {#magrittr}

Next to the packages that are directly contributed by tidyverse.org and are well known, there is
another, smaller one that is used during almost all examples in the **R for data science** book:
[magrittr](https://magrittr.tidyverse.org). *Magrittr* is automatically loaded when loading the
package *tidyverse* using `library(tidyverse)` that it is usually overseen that it has a longer
history on its own.

R is a functional programming language that means that it uses a programming paradigm that treats
computation as the evaluation of mathematical functions (see
[Wikipedia](https://en.wikipedia.org/wiki/Functional_programming) for more details). If we want to
obtain the unique elements of a vector, we use the function `unique()`:

```{r pipe1}
set.seed(1)
a <- sample(c("a", "b", "c"), 10, replace = TRUE)
unique(a)
```

If we want to additionally determine the number of unique elements, we can run the function
`length()` on the result of `unique(a)`. In order to do so, we have to **nest** these functions, by
first running `unique(a)` and then running `length()`:

```{r pipe2}
length(unique(a))
```

We can extend this nesting strategy by calling function after function, however, we have to
**construct the nesting from the inside out**. For example, we could have the following example:

```{r pipe3}
set.seed(1)
toupper(names(sort(table(sample(c("a", "b", "c", "d", "e", "f"), 1000, replace = T)), decreasing = T))[1:3])
```

<div class = "question">
<b>Questions:</b>
Which steps are we taking in the order? What is the result of the nested functions?
</div>

While nesting functions works well and we can obtain results from our multiple function constructs,
they are not intuitive to understand because we have to read them from the inside to the outside and
they are hard to comment.

In the scripting language **bash**, there is a special character to chain programs: the pipe
character `|`. Using the pipe character we can use the output of one program as the input of another
program without storing the intermediate data on the hard drive but keeping it in the memory. For
example, in order to count the number of files with the file extension "*.csv" in a directory, we
can write:

```{bash bash_pipe1, eval = F}
ls *.txt | wc -l
```

In R, this pipe operator `|` is already used for combining conditionals and represents the `OR`
operator for Boolean'ian operations. *Magrittr* introduces a new operator `%>%` with a very similar
function. Using `%>%`, we can use the output of one function as the input of another. If we take our
example from above, we can replace it with the following construct:

```{r pipe4}
library(magrittr)
set.seed(1)
c("a", "b", "c", "d", "e", "f") %>%  # take a vector with 6 lower-case characters
sample(1000, replace = T) %>%  # randomly draw 1,000 elements from this character vectors
table() %>%  # calculate the number of occurrences from each element
sort(decreasing = T) %>%  # sort occurrences descending
names() %>%  # identify the element names
.[1:3] %>%  # pick the top-3 most occurring ones
toupper()  # convert to upper-case
```

While we increased the vertical space and explained each step with a comment, we obtained the same
results by **concatenating the functions using** `%>%` as by using the nested function construct
above.

In its most basic form `x %>% f()` is identical to `f(x)`. If the function has additional arguments,
we can just add them, e.g. `x %>% sum(na.rm = T)`, and R will automatically understand that the
output of function on the left hand side of the pipe operator `%>%` will be used as the first
argument of the function on the right hand side of the pipe operator. *Magrittr* uses `.` as the
argument placeholder, in case there is no explicit function called like in our example to pick the
top-3 most occurring items `.[1:3]` or if we want to re-use it in function calls, e.g. `x %>% f(y =
nrow(.), z = ncol(.))`.

The same effect of having verbose code could be achieved without using pipe operator by **storing
the output of each indermediate step in its own variable**. While there is almost no change in the
length of code or its clarity, this option without the pipe operator comes with a caveat that I
would to illustrate here. Taking our example from above:

```{r pipe5}
set.seed(1)
characters <- c("a", "b", "c", "d", "e", "f")  # take a vector with 6 lower-case characters
random_sample <- sample(characters, 1000, replace = T)  # randomly draw 1,000 elements from this character vectors
element_counts <- table(random_sample)  # calculate the number of occurrences from each element
sorted_element_counts <- sort(element_counts, decreasing = T)  # sort occurrences descending
names_element_counts <- names(sorted_element_counts)  # identify the element names
top3_names_element_counts <- names_element_counts[1:3]  # pick the top-3 most occurring ones
toupper(top3_names_element_counts)  # convert to upper-case
```

<div class = "question">
<b>Questions:</b>
What is the caveat of this approach compared to using the pipe character?
</div>

While the results is still the same and we might have had to type more because of all the variable
names, the caveat becomes more apparent if we look at our workspace using the function `ls()`. If we
do so, we can see that we created six intermediate objects that we do not need any longer but still
exist in our workspace. In contrast, the approach using the pipe operator `%>%` did not create a
single R object because we did not store the intermediates by assigning it to variables or the
output result.

Having too many intermediate object in your R workspace makes it more difficult to distinguish the
important R objects with the final results from the intermediate helper constructs that are not
required any longer. While there is the option to remove R objects from the workspace using the
function `rm()`, the goal should be to avoid as many of them in the first place.

By typing

```{r pipe6}
rm("characters", "element_counts", "names_element_counts", "random_sample", "sorted_element_counts", "top3_names_element_counts")
ls()
```

we can remove the intermediate objects and restore our old workspace. Overall, this should not
discourage you from storing intermediate output in variables, because it will improve the debugging
of your code in case of error. Further, if commands are not directly chained like in a direct-graph
model but consist out of dependences, pipes will not be of much use either.

## tl;dr

The pipe operator `%>%` introduced by loading either the package `library(tidyverse)` or explicitly
`library(magrittr)` is a powerful tool that allows you **increase the readability of your code**,
while **avoiding the storage of intermediate R objects** in your workspace. This makes it concept
that should be heavily used throughout your data analysis.

## Resources

  - vignette of the R package *magrittr*: https://magrittr.tidyverse.org/
  - chapter 18 of "R for data science": https://r4ds.had.co.nz/pipes.html

# Data import using readr and data.table

In data analysis, the data are usually provided in a rectangular format, i.e. there is an equal
number of rows for every column in the table. The only commonly occurring exception is the parsing
of log files, for which we will need to decide on a line by line basis how to parse the code. For
most other cases, the base R function `read.table()` will help us.

## Base R: read.table()

Data import using regular base R functions like `read.table()` is tedious. Let's look at the
function in more detail by checking the default parameters of the function:

```{r dataimport1}
head(read.table, 8)
```

By printing a function or, in this case, the first eight lines of the function without providing
nothing else but the function name, R will show us the source code of the function. By default, the
following parameters are set:

  1. `header = FALSE`: data input has no header
  2. `sep = ""`: field separator is any white space (one or more spaces, tabs, newlines)
  3. `quote = "\""`: use double quote character to delineate strings
  4. `dec = "."`: define which character should be used for decimal point (. or ,)
  5. `numerals`: how to convert numerals into doubles when loss of accuracy is unavoidable
  6. `row.names`, `col.names`: specifies the row names (numeric when absent) and col names (numeric
                               with prefix V if absent) if no header is provided
  7. `as.is`, `colClasses`, `stringsAsFactors`: control the automatic conversion of character
     vectors into factors
  8. `na.strings = "NA"`: specify which strings should be encoded as `NA` after reading
  9. `nrows = -1`: number of lines to read in total; negative integer are ignored
  10. `skip = 0`: number of lines to skip from the beginning
  11. `check.names = TRUE`: if TRUE, corrects column and row names to fit R's variable name notation
  12. `fill`: if TRUE, fill rows with unequal length
  13. `strip.white = FALSE`: remove leading or trailing white spaces from each line
  14. `blank.lines.skip = TRUE`: if `TRUE`, skip blank lines in the input
  15. `comment.char`: character vector of length one that specifies which character to interpret as
      the start of a comment and to ignore all following text

These are the main parameters of importance and these are still quite a number. In order to have
more useful default parameters, base R has some additional functions that save you from defining all
these parameters over and over again.

  - `read.csv()`: read comma-separated file with header using "." as the decimal point character 
  - `read.csv2()`: read semicolon-separated file with header using "," as the decimal point
    character 
  - `read.delim()`: read tab-separated file with header using "." as the decimal point character
  - `read.delim2()`: read tab-separated file with header using "," as the decimal point character

In brief, all function names that have the suffix "2" are usually programmed for non-Anglo-American
default values of dealing with text files.

In order to read a text file, we can do the following:

```{r dataimport2}
df <- read.table("../00-crashcourse/txtfiles/data.tsv",
                 sep = "\t", header = T,
                 stringsAsFactors = F)
head(df)
```

In this example, I specified that we want to have the data parsed using the field separator `\t`,
while the first line contains the header. In order to save myself from typing it, I could also just
used the function `read.delim()` instead.

```{r dataimport3}
df <- read.delim("../00-crashcourse/txtfiles/data.tsv",
                 stringsAsFactors = F)
head(df)
```

The only additional parameter I have to specify still now is `stringsAsFactors = F`, in order to
avoid the conversion to factor (see the crashcourse slides for more details).

While `read.table()` and its derivates are decent every-day tools, I recommend you to use
different functions to read your data. In the following, I will introduce two of them, provided by
either the package `library(readr)` or `library(data.table)` that come with a few enhancements over
the base R ones.

## Readr: read_delim()/read_csv()/read_tsv()

The first alternative is based on the package `library(readr)` that is part of the tidyverse package
suit. While it provides more or less the very same base function, `read_delim()` instead of
`read.table()`, and the same derivates as the base R function, it has a few improvements:

  1. **faster speed**: typically up to 10x faster than base R; provides you with a progress bar for
     long jobs for reading a lot of lines
  2. **more reproducible**: no inheritance of variables from the operating system or environment
     (example: [NMR spectra off by 1%](https://arstechnica.com/information-technology/2019/10/chemists-discover-cross-platform-python-scripts-not-so-cross-platform/))
  3. **gets rid of common nuisances**: no character vectors as factors, row names always as numbers,
     no automatic changing of column names; returns data as [tibbles](#tibble)

If we want to re-use our example from above, we have to simply type:

```{r dataimport4, message = T, warning = T}
library(readr)
df <- read_tsv("../00-crashcourse/txtfiles/data.tsv")
```

When reading the data using the package `library(readr)`, the reading function guesses the
data type of the input and assigns it to the data type with the smallest set of allowed elements
(logical < numeric (double) < character) based by default on testing the first 1,000 elements of a
column. If we want to change this behaviour and assign a particular data type, e.g. we want the
column *control* to be considered as a factor, we can use the parameter `col_types`.

There are two different ways to provide the column types, the **compact specification** or using a
list. For the compact specification, **character** is abbreviated using a **c**, **double** using a
**d**, **logical** using a **l**, and **factor** using a **f**. To skip a column, we can use either
**-** or **\_**. For example, the compact specification of the current column types are "cddl". 

<div class = "question">
<b>Question:</b>
How does the compact specification look like if we want to specify the column control as factor?
</div>

Using the compact specification, we could use:

```{r dataimport5}
library(readr)
df <- read_tsv("../00-crashcourse/txtfiles/data.tsv",
               col_types = "cddf")
```

The specification using lists would look like this:

```{r dataimport6}
library(readr)
df <- read_tsv("../00-crashcourse/txtfiles/data.tsv",
               col_types = list(col_character(), col_double(), col_double(), col_factor()))
```

If you select a column type that does not fit the data present in the column, then readr will add
the attributes **problems** to a tibble, which we can inspect using `problems()`:

```{r dataimport7}
library(readr)
df <- read_tsv("../00-crashcourse/txtfiles/data.tsv",
               col_types = "cddd")
problems(df)
```

Here, we tried to read the data type **logical** as double, however, readr did not automatically use
the numeric representation of logical values (TRUE: 1, FALSE: 0) to replace the values. Instead, it
interpreted them as characters and set them `NA`.

If we want to convert explicitly, we could use the following thing:

```{r dataimport8}
library(tidyverse)
df <- read_tsv("../00-crashcourse/txtfiles/data.tsv") %>%
      mutate(control = as.numeric(control))
df
```

Next week, we will talk about in detail what we did here to achieve the conversion.

## Readr: write_csv()

Next to reading data into R, we often also want to store our results in a text file. While there is
a base R function `write.table()` that can do this for us, we can use the more efficient readr
functions `write_csv()` or `write_tsv()`.

Again, using readr instead of base R provides us with a few advantages:

  - **faster speed**: writing speed is up 2x
  - **sane defaults**: no row names, does not write column names when mode `append = T` is chosen
  - **allows compression**: by adding **.gz** (gzip), **.bz2** (bzip2), or **.xz** (lzma), we can
    have the output automatically compressed

To simply save our data frame as a tab-separated table to file:

```{r dataimport9, eval = F}
write_tsv(df, "study_design.txt")
```

## Data.table

Next to [Tidyverse](#tidyverse), there is another powerful package, **data.table**, that comes with
its own adaptation of the data frame structure and its own language to manipulate data. While it is
built for speed, its syntax is not very intuitive and therefore not very suited for beginners to
learn the basics of data analysis in R.

The reason why I will introduce it anyway here is that it is incredibly efficient in reading large
data sets. If you are working on files of multiple GB of size, using **readr** or **data.table**
will decrease the amount of time used.

Colin Gillespie and Robin Lovelace show in a benchmark test in their book ["Efficient R
Programming"](https://csgillespie.github.io/efficientR/input-output.html#fread) that if you have
issues loading large data sets, **data.table**'s function `fread()`.  

![Benchmarks of base, data.table and readr approches for reading csv files, using the functions read.csv(), fread() and read_csv(), respectively. ](https://csgillespie.github.io/efficientR/_main_files/figure-html/5-1-1.png)

Readr's functions like `read_csv()` have the particular draw back that they are more computationally
intensive when reading a table, due to parsing the columns, creating a tibble etc. Only with
increasing size of the data file, it fill reach the speed of `fread()` from data.table, which is
used as the baseline of this plot. In contrast, `read.table()` has a linear increase in run time
that is steeper the more columns are present in the table. However, one should keep in mind that the
necessary time is about 0.02 seconds for `fread()` and even a 13x increase in time takes less than a
second to load the data.

In order to read the data from our previous example, we run:

```{r dataimport10}
library(data.table)
df <- fread("../00-crashcourse/txtfiles/data.tsv")
head(df)
```

In contrast to `read_csv()`, it does not create an object of class **tibble** but **data.table**.
This would allow us to run data.table's own set of operations with high efficiency on the data but
does not prevent us from using the tidyverse functions as e.g. implemented in the package **dplyr**
or **tidyr**.

```{r dataimport11}
class(df)
```

## tl;dr

  - base R provides standard functions to read data, however, they are slow and have debatable
    default values
  - the package readr provides faster implementations of the same functions, e.g. `read_csv()` with
    reasonable defaults and returns R objects as **tibbles** that well integrate with the other
    tidyverse packages
  - for very large datasets (> thousands of lines), the package `data.table` provides a very
    powerful alternative with a strong advantage in speed

## Resources

  - chapter 11 of "R for data science": https://r4ds.had.co.nz/data-import.html
  - vignette of the R package: `data.table`: https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html
  - chapter 5.3 of "Efficient R programming": https://csgillespie.github.io/efficientR/input-output.html#fread

# Assignments

1. Read through the resources of the file format R Markdown and famliarise yourself with how to
   create an empty HTML document. Create an empty R Markdown file choosing a HTML document as output
   for this assignment.
2. Convert the following nested function into a chain of functions using the pipe operator `%>%` and
   explain each step using comments next to the pipe operator. If you do not know the R function,
   use the help function to understand its purpose.

```{r assignment2, eval=F}
# Set up
set.seed(0)
observation <- rnorm(100, mean = 100, sd = 10)
names(observation) <- sample(c("A", "B", "C", "D"), 100, replace = T)
# Nested function
chisq.test(matrix(table(names(observation)[observation >= mean(observation)]), nrow = 2, byrow = T))$p.value
```

3. Import the file `assignments/data.tsv` into R using the packages readr and data.table so that we
   obtain a four-column table with a header line and the column types: character, double, double,
   and logical. Why does the function `fread()` from data.table need fewer parameters?
